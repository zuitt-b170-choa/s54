let collection = [];

// Write the queue functions below.
// To test your skills, try to use pure javascript coding and avoid using pop, push, slice, splice, etc.

function print() {
	// output all the elements of the queue
    return collection;
}

function enqueue(element) {
	// add element to rear of queue
    let rear = collection.length;
    if(collection.length === 0){
        collection[0] = element;
    }
    else{
        collection[rear] = element;
    }
    return collection;
}

function dequeue() {
	// remove element at front of queue

    for (let i = 1; i < collection.length; i++){
        collection[i - 1] = collection[i];
        
    }
    collection.length--;

    return collection;
}

function front() {
	// show element at the front

    return collection[0];
}

function size() {
    // show total number of elements
    return collection.length;
}

function isEmpty() {
    // outputs Boolean value describing whether queue is empty or not
    if(collection.length === 0){
        return true;
    }
    else{
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
